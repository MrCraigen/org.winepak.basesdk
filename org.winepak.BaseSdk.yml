build-runtime: true

id: org.winepak.BaseSdk
id-platform: org.winepak.BasePlatform
branch: 3.0

runtime: org.freedesktop.Platform
runtime-version: 19.08
sdk: org.freedesktop.Sdk

sdk-extensions:
  - org.freedesktop.Sdk.Debug
  - org.freedesktop.Sdk.Locale
  - org.freedesktop.Sdk.Docs

add-extensions:
  org.winepak.BaseSdk.Docs:
    directory: share/runtime/docs
    bundle: true
    autodelete: true
    no-autodownload: true

platform-extensions:
  - org.freedesktop.Platform.Locale

inherit-extensions:
  - org.freedesktop.Platform.GL
  - org.freedesktop.Platform.Timezones
  - org.freedesktop.Platform.GStreamer
  - org.freedesktop.Platform.VAAPI.Intel
  - org.freedesktop.Sdk.Extension

finish-args:
  - --sdk=org.winepak.BaseSdk//3.0
  - --runtime=org.winepak.BasePlatform//3.0
  - --allow=multiarch
  - --env=PATH=/app/bin:/usr/bin:/usr/lib/i386-linux-gnu/bin
  - --env=XDG_DATA_DIRS=/app/share:/usr/share:/usr/share/runtime/share:/run/host/share
  - --env=WINEDEBUG=-all
  - --env=WINEPREFIX=/var/data/wine

cleanup:
  - /docs
  - /man
  - /share/man

cleanup-commands:
  - |
    if [ -L /usr/lib/i386-linux-gnu ]; then
      rm /usr/lib/i386-linux-gnu
      mkdir -p /usr/lib/i386-linux-gnu
    fi

cleanup-platform:
  - /docs
  - /share/aclocal
  - /share/pkgconfig
  - /lib/pkgconfig

cleanup-platform-commands:
  - |
    if [ -L /usr/lib/i386-linux-gnu ]; then
      rm /usr/lib/i386-linux-gnu
      mkdir -p /usr/lib/i386-linux-gnu
    fi

build-options:
  cflags: -O2 -pipe -fstack-protector-strong -fno-plt -frecord-gcc-switches -D_FORTIFY_SOURCE=2 -msse -msse2 -mfpmath=sse -msse3 -mssse3 -msahf -mprfchw -mcx16
  cxxflags: -O2 -pipe -fstack-protector-strong -fno-plt -frecord-gcc-switches -D_FORTIFY_SOURCE=2 -msse -msse2 -mfpmath=sse -msse3 -mssse3 -msahf -mprfchw -mcx16
  ldflags: -fstack-protector-strong -Wl,-z,relro,-z,now
  env:
    V: '1'
  arch:
    x86_64:
      libdir: /usr/lib/x86_64-linux-gnu
    i386:
      libdir: /usr/lib/i386-linux-gnu

modules:
  - name: platform-setup
    buildsystem: simple
    build-commands:
      - mkdir -p /usr/share/runtime/docs
      - mkdir -p /usr/lib/sdk
      - mkdir -p /usr/lib/extension
      - mkdir -p /usr/lib/gecko
      - mkdir -p /usr/lib/mono

  - "mingw.yml"
  - "shared-modules/glu/glu-9.json"

  - name: pcap
    sources:
      - type: archive
        url: https://www.tcpdump.org/release/libpcap-1.9.0.tar.gz
        sha256: 2edb88808e5913fdaa8e9c1fcaf272e19b2485338742b5074b9fe44d68f37019

  - name: v4l-utils
    config-opts:
      - --disable-static
      - --disable-doxygen-doc
      - --disable-v4l-utils
    sources:
      - type: archive
        url: https://linuxtv.org/downloads/v4l-utils/v4l-utils-1.16.7.tar.bz2
        sha256: ee917a7e1af72c60c0532d9fdb9e48baf641d427aa7b009a9b2ca821f9e8e0d9

  - name: capi
    sources:
      - type: archive
        url: "http://deb.debian.org/debian/pool/main/libc/libcapi20-3/libcapi20-3_3.27.orig.tar.bz2"
        sha256: d8e423d5adba1750f511a2c088296db2a8a2e1e9209401871b01ce411d8ac583

  - name: sane
    config-opts:
    # from aur
    - --enable-libusb_1_0
    - --enable-pthread
    - --enable-avahi
    - --disable-locking
    - --disable-rpath
    sources:
      - type: archive
        url: https://gitlab.com/sane-project/backends/uploads/9e718daff347826f4cfe21126c8d5091/sane-backends-1.0.28.tar.gz
        sha256: 31260f3f72d82ac1661c62c5a4468410b89fb2b4a811dabbfcc0350c1346de03

  - name: gsm
    buildsystem: simple
    build-commands:
      - patch -p0 -i gsm-shared.patch
      - install -m755 -d /usr/include/gsm
      - make CCFLAGS="-c ${CFLAGS} -fPIC"
      - make -j1 INSTALL_ROOT=/usr GSM_INSTALL_INC=/usr/include/gsm install
    sources:
      - type: archive
        url: http://www.quut.com/gsm/gsm-1.0.18.tar.gz
        sha256: 04f68087c3348bf156b78d59f4d8aff545da7f6e14f33be8f47d33f4efae2a10
      - type: file
        path: gsm/gsm-shared.patch
        sha256: 412e26ab0a0fe187f7a8a16400921ef745a8af5440006136e4d3169d30875f50

  - name: openldap
    config-opts:
      - --disable-static
      - --disable-bdb
      - --disable-hdb
      - --enable-mdb
    sources:
      # OpenLDAP required for Wine
      # Due to the licencing of BDB v6.* we use LMDB
      # FTP isn't support in flatpak & flatpak-builder so we use he https mirror
      # https://www.openldap.org/software/download/OpenLDAP/openldap-release/
      - type: archive
        url: https://www.openldap.org/software/download/OpenLDAP/openldap-release/openldap-2.4.46.tgz
        sha256: 9a90dcb86b99ae790ccab93b7585a31fbcbeec8c94bf0f7ab0ca0a87ea0c4b2d
    modules:
      - name: lmdb
        subdir: libraries/liblmdb
        no-autogen: true
        make-install-args:
          - prefix=/usr
        sources:
          - type: archive
            url: https://github.com/LMDB/lmdb/archive/LMDB_0.9.24.tar.gz
            sha256: 44602436c52c29d4f301f55f6fd8115f945469b868348e3cddaf91ab2473ea26

  - name: krb5
    subdir: src
    config-opts:
      - --prefix=/usr
      - --enable-shared
      - --with-ldap
      - --with-lmdb
    sources:
      - type: archive
        url: https://web.mit.edu/kerberos/dist/krb5/1.17/krb5-1.17.tar.gz
        sha256: 5a6e2284a53de5702d3dc2be3b9339c963f9b5397d3fbbc53beb249380a781f5

  - name: gphoto2
    sources:
      - type: archive
        url: https://downloads.sourceforge.net/project/gphoto/libgphoto/2.5.23/libgphoto2-2.5.23.tar.bz2
        sha256: d8af23364aa40fd8607f7e073df74e7ace05582f4ba13f1724d12d3c97e8852d

  - name: opencl-headers
    buildsystem: simple
    build-commands:
      - cp -R CL /usr/include/CL
    sources:
      - type: git
        url: https://github.com/KhronosGroup/OpenCL-Headers.git
        commit: 0d5f18c6e7196863bc1557a693f1509adfcee056

  - name: FAudio #https://www.winehq.org/announce/4.3
    buildsystem: cmake-ninja
    config-opts:
      - -DCMAKE_BUILD_TYPE=Release
      - -DFFMPEG=ON
    sources:
      - type: archive
        url: "https://github.com/FNA-XNA/FAudio/archive/19.09.tar.gz"
        sha256: 754b5ccc239bc1be4046d254748cface915e653aec179424fd452255a7082677

  - name: cabextract
    buildsystem: autotools
    sources:
      - type: archive
        url: https://www.cabextract.org.uk/cabextract-1.6.tar.gz
        sha256: cee661b56555350d26943c5e127fc75dd290b7f75689d5ebc1f04957c4af55fb

  - name: unrar
    no-autogen: true
    sources:
      - type: archive
        url: https://www.rarlab.com/rar/unrarsrc-5.6.4.tar.gz
        sha256: 9335d2201870f2034007c04be80e00f1dc23932cb88b329d55c76134e6ba49fe

  - name: unshield
    buildsystem: cmake
    sources:
      - type: archive
        url: https://github.com/twogood/unshield/archive/1.4.3.tar.gz
        sha256: aa8c978dc0eb1158d266eaddcd1852d6d71620ddfc82807fe4bf2e19022b7bab

  - name: metainfo
    disabled: true
    buildsystem: simple
    build-commands:
      - mkdir -p /usr/share/appdata
      - install org.winepak.Platform.appdata.xml /usr/share/appdata
      - install org.winepak.Sdk.appdata.xml /usr/share/appdata
      - appstream-compose --basename=org.winepak.BasePlatform --prefix=/usr --origin=flatpak org.winepak.Platform
      - appstream-compose --basename=org.winepak.BaseSdk --prefix=/usr --origin=flatpak org.winepak.Sdk
    sources:
      - type: file
        path: org.winepak.Sdk.appdata.xml
      - type: file
        path: org.winepak.Platform.appdata.xml
